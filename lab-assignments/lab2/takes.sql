CREATE TABLE takes (
	ID varchar(5) NOT NULL,
	course_id varchar(8) NOT NULL,
	sec_id varchar(8) NOT NULL,
	semester varchar(6) NOT NULL,
	year decimal(4,0) NOT NULL,
	grade varchar(2) NOT NULL,
	
	FOREIGN KEY (ID) references student (ID),
	FOREIGN KEY (course_id, sec_id, semester, year) references section (course_id, sec_id, semester, year)
on delete cascade
on update cascade
	
	PRIMARY KEY (ID, course_id)
	
	);

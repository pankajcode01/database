CREATE TABLE teaches (
	ID varchar(5) NOT NULL,
	course_id varchar(8) NOT NULL,
	sec_id varchar(8) NOT NULL,
	semester varchar(6) NOT NULL,
	year decimal(4,0) NOT NULL,
	
	FOREIGN KEY (ID) references instructor (ID),
	FOREIGN KEY (course_id, sec_id, semester, year) references section (course_id, sec_id, semester, year),
	PRIMARY KEY (ID, course_id, sec_id, semester, year)
	
	);
